package utils

import "encoding/json"

type Response struct {
	Message string
	Code    int
}

// BuildResponse return a byte array for will be used in a HTTP server.
func BuildResponse(messsage string, code int) []byte {
	response := Response{
		Message: messsage,
		Code:    code,
	}

	toReturn, err := json.Marshal(response)

	if err != nil {
		return []byte("Error Marshal.")
	}

	return toReturn
}
