package main

import (
	"net/http"

	"gitlab.com/kadev-psb/utils-tools"
)

type Response struct {
	Message string `json: "message"`
	Code    int    `json: "code"`
}

func main() {
	http.HandleFunc("/", HelloServer)
	http.ListenAndServe(":8080", nil)
}

func HelloServer(w http.ResponseWriter, r *http.Request) {
	response := utils.BuildResponse("Ok", 1)
	w.Write(response)
}
